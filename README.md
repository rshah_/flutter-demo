## Info on this basic calculator Application.

Well, this is my first step to using the learned widgets and the UI techniques from **Fulutter Docs**. In case you might get confused on the lines preceding the logic around the widget, I will be adding the comment very soon explaning each section.

---

## Before you clone this repo and try to make it work.

*This is just an example of simple flutter widget usage. So if you are trying it out in your IDE(VS Code or Android Studio) there might be one or more reason you might be getting errors to get the result. Hence for the same please read the following para*

---

**Steps to run the main dart**

1. *Copy the entire code from main.dart*

2. *Create another main.dart in a fresh project you wish to   run*

3. *Replace the old code in the main by pasting the copied one*

4. *Save the main.dart and try runnning the code, do not forget to activate a device*

5. *You might need to hot reload even after building the project, for the same you must be patient and do SHIFT + R in the terminal*



