import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Calculator',
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double firstnum;
  double secondnum;
  String texttodisplay;
  String result;
  String operation;

  void buttonclicked(String btn) {
    if (btn == "C") {
      texttodisplay = "";
      firstnum = 0;
      secondnum = 0;
      result = "";
    } else if (btn == "+" || btn == "-" || btn == "x" || btn == "/") {
      firstnum = double.parse(texttodisplay);
      result = "";
      operation = btn;
    } else if (btn == "=") {
      secondnum = double.parse(texttodisplay);
      if (operation == "+") {
        result = (firstnum + secondnum).toString();
      }
      if (operation == "-") {
        result = (firstnum - secondnum).toString();
      }
      if (operation == "x") {
        result = (firstnum * secondnum).toString();
      }
      if (operation == "/") {
        result = (firstnum ~/ secondnum).toString();
      }
    } else {
      result = int.parse(texttodisplay + btn).toString();
    }

    setState(() {
      texttodisplay = result;
    });
  }

  Widget custombutton(String btnval) {
    return Expanded(
      child: OutlineButton(
        padding: EdgeInsets.all(30.0),
        onPressed: () => buttonclicked(btnval),
        child: Text(
          '$btnval',
          style: TextStyle(fontSize: 40.0, color: Colors.indigo),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculator"),
      ),
      body: Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Expanded(
            child: Container(
                padding: EdgeInsets.all(20.0),
                alignment: Alignment.bottomRight,
                child: Text(
                  "$texttodisplay",
                  style: TextStyle(
                    fontSize: 25.0,
                    fontWeight: FontWeight.w600,
                    color: Colors.indigo,
                  ),
                )),
          ),
          Row(
            children: <Widget>[
              custombutton("9"),
              custombutton("8"),
              custombutton("7"),
              custombutton("+"),
            ],
          ),
          Row(
            children: <Widget>[
              custombutton("6"),
              custombutton("5"),
              custombutton("4"),
              custombutton("-"),
            ],
          ),
          Row(
            children: <Widget>[
              custombutton("3"),
              custombutton("2"),
              custombutton("1"),
              custombutton("x"),
            ],
          ),
          Row(
            children: <Widget>[
              custombutton("C"),
              custombutton("0"),
              custombutton("="),
              custombutton("/"),
            ],
          )
        ],
      )),
    );
  }
}
